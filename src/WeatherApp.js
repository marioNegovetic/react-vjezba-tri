import React, { useState } from 'react';
import axios from 'axios';
import './App.css';

export default function WeatherApp(){
    const [city, setCity] = useState('');
    const [res, setRes] = useState({temper: 0, conditionText: '', conditionPic: '',wind: 0, pressure: 0, humidity: 0, visability: 0});
    const [mood, setMood] = useState({Rijeka: '', Zagreb: '', Split:'', Rome:'', Berlin:'', Helsinki:'', Istanbul:''});
    const [moodColor, setMoodColor] = useState({Rijeka: '', Zagreb: '', Split:'', Rome:'', Berlin:'', Helsinki:'', Istanbul:''});
   

    async function WeatherInfo(e){
        setCity(e.target.value);
        let resu = await axios.get('http://api.weatherapi.com/v1/current.json?key=264432be96fd48df990135151210508&q=' + e.target.value + '&aqi=no');
        let rez = resu.data.current;
        setRes({temper: rez.temp_c, conditionText: rez.condition.text, conditionPic:rez.condition.icon, wind: rez.wind_mph, pressure: rez.pressure_mb,
            humidity: rez.humidity, visability: rez.vis_km});
        console.log(rez);
        
    }
    
     const changeStatus = (e) =>{
         const color = e.target.value;
        setMood({...mood, [city]: e.target.value });
        console.log(color);
        
        if(color === 'great'){
            setMoodColor({...moodColor, [city]:'green'});
        } else if(color === 'not-so-great'){
            setMoodColor({...moodColor, [city]:'yellow'});
        } else if(color === 'okay-ish'){
            setMoodColor({...moodColor, [city]:'blue'});
        } else if(color === 'sad'){
            setMoodColor({...moodColor, [city]:'pink'});
        }
    };
    return(
        <div style={{backgroundColor: moodColor[city]}} className="App-header">
            <h3>Please choose city: </h3>
            <select name="city" onChange={WeatherInfo}>
                <option value="">City</option>
                <option value="Rijeka">Rijeka</option>
                <option value="Zagreb">Zagreb</option>
                <option value="Split">Split </option>
                <option value="Rome">Rome </option>
                <option value="Berlin">Berlin </option>
                <option value="Helsinki">Helsinki </option>
                <option value="Istanbul">Istanbul </option>
            </select>
            <h2>Current city is: {city}</h2>
            <h2>Current mood is: {mood[city]}</h2>
            <table>
               
                <thead>
                    <tr>
                        <th>Temperature</th>
                        <th>Weather condition</th>
                        <th>Weather picture</th>
                        <th>Wind speed</th>
                        <th>Pressure</th>
                        <th>Humidity</th>
                        <th>Visability</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{res.temper}</td>
                        <td>{res.conditionText}</td>
                        <td><img src={res.conditionPic} alt=""/></td>
                        <td>{res.wind}</td>
                        <td>{res.pressure}</td>
                        <td>{res.humidity}</td>
                        <td>{res.visability}</td>
                    </tr>
                </tbody>   
            </table>
            
                <button value={'great'} onClick={changeStatus}>Great 
                <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" fill="currentColor" className="bi bi-emoji-smile-fill" viewBox="0 0 16 16">
                <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zM7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zM4.285 9.567a.5.5 0 0 1 .683.183A3.498 3.498 0 0 0 8 11.5a3.498 3.498 0 0 0 3.032-1.75.5.5 0 1 1 .866.5A4.498 4.498 0 0 1 8 12.5a4.498 4.498 0 0 1-3.898-2.25.5.5 0 0 1 .183-.683zM10 8c-.552 0-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5S10.552 8 10 8z"/>
                </svg></button>
                <button value={'not-so-great'} onClick={changeStatus}>Not so great
                <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" fill="currentColor" className="bi bi-emoji-expressionless-fill" viewBox="0 0 16 16">
                <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zM4.5 6h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1zm5 0h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1zm-5 4h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1 0-1z"/>
                </svg></button>
                <button value={'okay-ish'} onClick={changeStatus}>Okay-ish
                <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" fill="currentColor" className="bi bi-emoji-neutral-fill" viewBox="0 0 16 16">
                <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zM7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm-3 4a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zM10 8c-.552 0-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5S10.552 8 10 8z"/>
                </svg></button>
                <button value={'sad'} onClick={changeStatus}>Sad
                <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" fill="currentColor" className="bi bi-emoji-frown-fill" viewBox="0 0 16 16">
                <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zM7 6.5C7 7.328 6.552 8 6 8s-1-.672-1-1.5S5.448 5 6 5s1 .672 1 1.5zm-2.715 5.933a.5.5 0 0 1-.183-.683A4.498 4.498 0 0 1 8 9.5a4.5 4.5 0 0 1 3.898 2.25.5.5 0 0 1-.866.5A3.498 3.498 0 0 0 8 10.5a3.498 3.498 0 0 0-3.032 1.75.5.5 0 0 1-.683.183zM10 8c-.552 0-1-.672-1-1.5S9.448 5 10 5s1 .672 1 1.5S10.552 8 10 8z"/>
                </svg></button>          
        </div>         
    );   
}